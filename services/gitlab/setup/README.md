---
Title: GitLab Account Setup
Subtitle: Git Hosting on GitLab FTW
Query: true
---

GitLab is currently the [best](../isbest/) [Git](https://duck.com/lite?q=Git) hosting provider. Setting up an account on GitLab is easy.

