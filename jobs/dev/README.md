---
Title: Software Developer
Subtitle: Fastest Growing Tech Profession Per Income
Query: true
---

[Software Developer](https://www.bls.gov/ooh/computer-and-information-technology/information-security-analysts.htm) (aka [Software Engineer]) is the second fastest growing [technology occupation](/jobs/).

## Software Engineer

The origin of this title is an software developer working at NASA who wanted to be taken more seriously and so started calling themselves an "engineer" triggering the other engineers and forever giving license to people who write code all day to call themselves engineers like everyone else.
