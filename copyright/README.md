---
Title: Copyright and Licensing
---

As an [open knowledge](/what/knowledge/) project priority is on making this content available to everyone and anyone --- in whatever way they can use it --- including commercially --- and to ensure that *any* change or alternation or improvement is available to everyone else as well. 

Therefore, except where otherwise noted, the published content is copyright 2020 Rob Muhlestein and licensed to all under a [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license. This includes the code included in any part of the content. No warranty of any kind is provided with that code since it is simply a part of the written content.

:::co-tip
If you need to create a PDF of any specific page the printed copyright at the bottom of each page should fulfill these requirements if unaltered.
:::

This means you are free to do the following:

* Copy and redistribute the material in any medium or format

* Remix, transform, and build upon the material for any purpose, even commercially

So long as you do the following:

* You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

* If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

* You may not apply legal terms or technological measures that legally restrict others from doing anything this license permits.

## "All Rights Reserved" is Defunct

Did you know you have not needed to put *all rights reserved* on anything for several years now?

The words have [“no legal significance” according to Ius mentis](https://duck.com/lite?kae=t&q=“no legal significance” according to Ius mentis) and originated because of an obscure treaty called the *Buenos Aires Copyright Convention* which has been superseded by the *Berne Convention*. 

So to all those out there who still think you need them, you don't.

## Trademarks

The following --- and their equal with inverted colors --- are trademarks of Rob Muhlestein:

* [RWX.GG]{.spy}
* `rwx.gg`
* `rwxrob.live`
* Linux Beginner Boost
