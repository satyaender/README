---
Title: Impostor Syndrome
Subtitle: You Actually Don't Suck
Query: true
---

> "One of the painful things about our time is that those who feel certainty are stupid, and those with any imagination and understanding are filled with doubt and indecision." (Bertrand Russell)

*Impostor syndrome* is when you think you suck despite all the evidence to the contrary. You feel like any success you have is undeserved and somehow you are a fraud. 

Many independent learners struggle with impostor syndrome, which is actually a good thing. It means you are constantly challenging your skills and knowledge and pushing yourself even though you should eventually learn to accurately assess your skills and knowledge to gain authentic confidence in your achievements. 

The opposite of impostor syndrome is [Dunning-Kruger effect](/what/learning/cognitive/dk/) which often comes with [cognitive dissonance](/what/learning/cognitive/diss/) and is much more dangerous, to everyone.

## See Also

* [I Suck. I Rock!](https://youtu.be/cFIF46j59LY)
* [Overcoming Imposter Syndrome (Harvard Business Review)](https://hbr.org/2008/05/overcoming-imposter-syndrome)
