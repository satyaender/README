---
Title: Cognitive Biases
Subtitle: Everyone Has Them
Query: true
---

A *cognitive bias* is a mistake in reasoning, evaluating, remembering,
or other thinking that comes from a person holding onto their beliefs
regardless of information to the contrary. In extreme cases a person
might block that information entirely or immediately forget it.
[Cognitive dissonance](/what/learning/cognitive/diss/) is one reason
someone might have a cognitive bias.
