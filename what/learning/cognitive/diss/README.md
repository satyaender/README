---
Title: Cognitive Dissonance
Subtitle: Your Mind Protecting Itself from Reality
Query: true
---

> "I feel I own you an apology, Neo. We have a rule. We never free a mind once it's reached a certain age. It's dangerous. The mind has trouble letting go. I've seen it before and I'm sorry. I did what I did because I had to." (Morpheus, The Matrix, 0:44:44)

*Cognitive dissonance* is the state of being when a person freaks out because most or all of their reality turns out to be fake. If intense enough people can literally go crazy from it, or lash out and call *the reality* "fake" rather than accept it. CogDiss is usually a subconscious mental defense mechanism people won't even realize or accept. 

Often CogDiss turns into [Dunning-Kruger Effect](/what/learning/cognitive/dk/). You should constantly check yourself with as much objective validation as possible, no matter how convinced you are that you are right or that your work is perfect --- especially as a technologist. If not, people may [die](/what/spaghetti/).

:::co-fyi
Cognitive dissonance is the scientific term for what Plato allegorically illustrates in [Plato's Cave](https://duck.com/lite?kae=t&q=Plato's Cave).
:::
