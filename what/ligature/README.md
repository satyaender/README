---
Title: What is a Ligature?
Subtitle: When Two Characters are Combined Into One
Query: true
---

A *ligature* is when two characters are combined into a single new character. So for example with `>=` is combined into ``≥.
