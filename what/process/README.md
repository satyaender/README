---
Title: What is a Computer Process?
Subtitle: A Running Program
Query: true
---

A *process* is simply a running program started either directly by a [user](/what/users/) or indirectly by the system itself.

:::co-tip
Use the `top` command to see the running processes in real time.
:::
