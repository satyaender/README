---
Title: Vi/m Tasks
Query: true
---

Command Mode

* Move Down One Line
* Change a Single Word
* Save and Exit

Insert Mode

* Move Down One Line in Insert Mode

