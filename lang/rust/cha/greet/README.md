---
Title: Rust Greeter
Subtitle: Create a Greeter Script with Arguments in Rust
Catagory: Challenge
Summary:
  Create a Rust program that says hello to a specific person either by first name (`./greet First`) or full name (`./greet First Last`) including quoted full names (`./greet 'First Name'`).
Prereqs:
- /lang/bash/cha/hello/
---
