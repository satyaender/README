---
Title: Bash Mnemonic Mini-Projects
Subtitle: Exercise Those Skills. 
---

These [MMPs](/what/cha/) gradually introduce you to more complicated elements of Bash scripting. You will likely have the most success going through them in order.

:::co-btw

Remember these projects are not designed to be something you would put on your resume. They are deliberately small and silly to help you remember and master the concepts and skills they cover so that you can make other *real* projects later that you *should* add to your portfolio.
:::


Table Beginner Bash Projects

### Intermediate

* `rw`
* TwitchTool (`curl`, web API, GraphQL)
* RepoTool
* PDFViewer
* SpotifyTool
* `.bashrc`
