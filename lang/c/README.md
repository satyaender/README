---
Title: C Programming Language
Subtitle: Created to Create UNIX
Query: true
---

The C programming language is mandatory learning for anyone who truly wants to understand how *all* languages and digital computers work. While it is not the easiest language to learn, it is by far the most significant language ever invented. Every major language and operating system started out in C including the re-write of UNIX from Assembly. Mastering C fundamentals will significantly boost your learning of *any* other language later.

## RWX C Coding Style

The `rwx.gg` project follows a modern C coding style that is more consistent with that of JavaScript, Go, and Rust. It is the same as the [Linux Kernel Coding Style](/reviews/books/kernstyle/) with the following changes that are also followed in the book [Modern C](/reviews/books/modernc/):

* Left curly [brackets](/key/) *always* end of line.
* *All* blocks require curly brackets.
* Indent two spaces.

All the other great advice about size and scope of functions and such still very much applies.

### Motivation

* Remain consistent with JavaScript, Go, and Rust
* Present C clearly on very small screens
* Maintain the spirit of K&R style
