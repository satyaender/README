---
Title: Course and Session Structure
---

Those in the course meet virtually on Twitch every weekday from 11am to 2pm for 16 weeks after which time the course repeats. Every hour there is a 10-minute break at the *beginning* of the hour but everyone is welcome to continue discussion during that time.

The course follows a *flipped* model meaning that most of the learning and work is done *outside* of course sessions *not* during them. Sessions are therefore a place to discuss material, exercises, and projects with the mentor and group. *You are your own instructor.* Your mentor and community are here to help and support you as you teach yourself. 

Content is organized by *week* (not session) allowing maximum flexibility during each weekly session. There is nothing limiting anyone from learning on their own well before or after any given week. Any topic listed for the given week is open for discussion. Off-topic discussion will generally not be allowed and most sessions will be moderated to ensure discussion remains productive. Unrelated comments during this time will result in a Twitch "time-out" unless the session completes.
