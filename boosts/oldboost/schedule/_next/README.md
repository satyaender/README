---
Title: Course Content Schedule (Next)
Subtitle: What When
---

*This is the revised schedule that will be used for the next boost.*

Content is organized by [weeks](/boosts/oldboost/calendar/), weeks by days, days by [sessions](/courses/boost/times/). Current week is highlighted. Topics and skills are included that are required by *all* [target occupations](/jobs/). As with everything in this [knowledge app](/what/knowledge/apps/) the schedule and content will change frequently. Check back often.

1. Day One
  1. RWX: In Memory of Aaron Swartz
  1. Intro, Prereqs, Logistics, Goals
  1. ProtonMail, GitLab, Notes, Netlify
1. Day Two
  1. Basic Pandoc Markdown
  1. Basic HTML
  1. Basic CSS
1. Day Three
  1. Intro to Structured Data, JSON
  1. YAML for Humans
  1. Delimited, CSV, TOML, XML
1. Day Four
  1. Get Linux: WSL, VirtualBox, Cloud
  1. Terminal Survivor
  1. Get By with Vi
1. Day Five
  1. Configure Local Git Repos
  1. Clone GitLab Notes and `dotfiles`
  1. Git for Backups
1. Day Six
  1. Portable Bash Configuration
  1. Bash MMP: `greet`
1. Day Seven
  1. Portable Vi Configuration
  1. Professional Vi/Bash Integration
  1. Bash MMP: `now`
  1. Bash MMP: `cmt`
1. Day Eight
  1. Bash MMP: `nyan`
  1. Bash MMP: `rndcolor`
  1. Bash MMP: `hex2rgb`
1. Day Nine
  1. Bash MMP: `waffles`
1. Day Ten
  1. Bash MMP: `badgers`  
1. Day Eleven:
  1. Bash MMP: `eightball`
1. Day 12
  1. Bash MMP: `roll`

  1. Bash MMP: `bridgekeeper`
  1. Python MMP: `bridgekeeper`
1. Day Twelve:
  

1. Now What
    1. Get Good to Gig 
    1. Stay Prescient
    1. Make Contact
    1. Follow Your Favorites
